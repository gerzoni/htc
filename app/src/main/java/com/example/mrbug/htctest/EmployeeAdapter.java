package com.example.mrbug.htctest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EmployeeAdapter extends BaseAdapter {
    private List<Employee> list;

    public EmployeeAdapter(List<Employee> list){
        this.list = list;
        //Сортируем список по алфавиту
        Collections.sort(list, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.item_layout, parent, false);
        }

        Employee emp = (Employee) getItem(position);

        TextView nameEmpTv = view.findViewById(R.id.nameEmpTv);
        TextView phoneEmpTv = view.findViewById(R.id.phoneEmpTv);
        TextView skillsEmpTv = view.findViewById(R.id.skillsEmpTv);

        nameEmpTv.setText(emp.getName());
        phoneEmpTv.setText(emp.getPhone_number());
        skillsEmpTv.setText(emp.getSkills().toString());

        return view;
    }
}
