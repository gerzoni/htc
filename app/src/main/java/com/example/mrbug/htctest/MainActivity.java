package com.example.mrbug.htctest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ResponseBody responseBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView nameTv = findViewById(R.id.nameTv);
        final TextView ageTv = findViewById(R.id.ageTv);
        final TextView competencesTv = findViewById(R.id.competencesTv);
        final ListView listView = findViewById(R.id.listView);

        //Получаем данные от сервера
        getApiService().getCompany().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() != null){
                    responseBody = response.body();
                    Company company = responseBody.getCompany();

                    nameTv.setText(company.getName());
                    ageTv.setText(String.valueOf(company.getAge()));
                    competencesTv.setText(company.getCompetences().toString());

                    final EmployeeAdapter adapter = new EmployeeAdapter(company.getEmployees());
                    listView.setAdapter(adapter);
                }
                else {
                    Log.d("", "Получено пустое тело ответа");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("", "Что-то пошло не так.");
            }
        });
    }

    public ApiService getApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }
}
