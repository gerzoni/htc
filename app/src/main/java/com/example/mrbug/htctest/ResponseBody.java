package com.example.mrbug.htctest;

public class ResponseBody {
    private Company company;

    public Company getCompany() {
        return company;
    }

    public ResponseBody(Company company) {
        this.company = company;
    }
}
