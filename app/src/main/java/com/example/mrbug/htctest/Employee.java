package com.example.mrbug.htctest;

import java.util.List;

public class Employee {

    private String name;
    private String phone_number;
    private List<String> skills;

    public Employee(String name, String phone_number, List<String> skills) {
        this.name = name;
        this.phone_number = phone_number;
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }
}
