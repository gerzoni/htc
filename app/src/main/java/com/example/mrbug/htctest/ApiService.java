package com.example.mrbug.htctest;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("56fa31e0110000f920a72134")
    Call<ResponseBody> getCompany();
}
